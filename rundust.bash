#!/bin/bash
# -------------------------------------------------------
# Adapted from
# dust-profile.csh by Simon Vaughan 2005
# shell script to extract dust n(D) profile from
# CCD (imaging mode) event list)
# uses: 
#   FtOOLS
#   dust-profile.f90

# Define what to do if interrupted


srcdir=`pwd`

# Initialise FTOOLS if not already done

# define data parameters
if [[ $# -lt 7 ]]; then		
	    echo \*\* Need to specify :
        echo \*\* \(1\) FITS event file to use
        echo \*\* \(2\) x0 - detector x-position of GRB
        echo \*\* \(3\) y0 - detector y-position of GRB
        echo \*\* \(4\) t0 - burst time of GRB \(sec\)
        echo \*\* \(5\) tmax - max. time since GRB to use \(sec\)
        echo \*\* \(6\) arcsec per pixel \(e.g. 2.357 for Swift XRT\)
        echo "** (7) dust-profile program"
        exit 1
else
        evtfile=$1
        cenX=$2
        cenY=$3
        t0=$4
        tmax=$5
        arcppix=$6
		dustprofileprog=$7
fi


localfname=$(basename $evtfile)

oevtfile=${localfname%.evt}.t0
echo fcalc infile=$evtfile+1 outfile=$oevtfile  clname="TIME" rowrange="-" inull=0 copycol=yes clobber=yes expr="TIME-$t0"
fcalc infile=$evtfile+1 outfile=$oevtfile  clname="TIME" rowrange="-" inull=0 copycol=yes clobber=yes expr="TIME-$t0"


# convert FITS event file to ASCII table
if [[ -e $evtfile ]]; then
		echo fdump infile=$oevtfile+1 outfile=temp.txt columns="TIME,X,Y" rows="-" prhead="no" showcol="no" showrow="no" showunit="no" clobber=yes
        fdump infile=$oevtfile+1 outfile=temp.txt columns="TIME,X,Y" rows="-" prhead="no" showcol="no" showrow="no" showunit="no" clobber=yes
else
        echo \*\* Cannot find file : $evtfile
        exit 2
fi

# remove leading two blank lines
outf=${localfname%.evt}.txt
tail -n+3 temp.txt > $outf

# count number of events
ne=$(wc -l $outf)
ne=($ne)
ne=${ne[0]}

echo "Number of events: $ne"

# remove output file if exists
outp=${localfname%.evt}.dat
rm $outp


# write parameter file for dust-profile.f90
profile=${srcdir}/dust-profile.par
cat > $profile <<EOF
$outf
$ne
$cenX
$cenY
$tmax
$arcppix
$outp
EOF


# then run dust-profile.f90 to calculate n(D) distribution
echo $dustprofileprog
$dustprofileprog

# convert output to XSPEC format
fpha=${localfname%.evt}.pha
frsp=${localfname%.evt}.rsp

if [[ -e $outp ]]; then
		echo "flx2xsp infile=$outp phafil=$fpha rspfil=$frsp clobber=yes"
	    flx2xsp infile=$outp phafil=$fpha rspfil=$frsp clobber=yes
else
        echo \*\* No output ASCII file made by bin-profile.f90
        exit 3
fi
  
# load into XSPEC and plot
xcmfile=${localfname%.evt}.xcm
cat > $xcmfile <<EOF
data ${fpha}
setplot energy
setplot command lw 3
setplot command font roman
setplot command la x Distance (pc)
setplot command la y Counts (pc \\\u-1\\\d)
setplot command line step
setplot command re x
setplot command re y
plot ldata
setplot command hardcopy /ps
plot ldata
exit
EOF


if [[ -e ${fpha} ]]
then
        xspec - $xcmfile > /dev/null
else
		echo "No PHA file available"
		exit 4
fi

# save output plot file
psfile=${localfname%.evt}.ps
if [[ -e pgplot.ps ]]; then
	    mv pgplot.ps $psfile
else
        echo \*\* No PSD produced
fi


# all done
echo -- All done
