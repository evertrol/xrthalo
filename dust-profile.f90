! Code to process photon event table and
! produce n(D) dust column distribution 
! as in Tiengo & Mereghetti (2005)
! (c) 2005, Simon Vaughan, Leicester
!
! cpbin = number of counts per bin (e.g. N>20)
! n = total number of events
! nt = number of events with [t < tmax]
! nb = number of bins 

PROGRAM dustprofile

  implicit none
  integer, parameter              :: cpbin=20
  integer                         :: n, ierr, nb, nt
  real                            :: cenx, ceny, tmax, arcppix
  real, dimension(:), allocatable :: x,y,t,d
  real, dimension(:), allocatable :: d1,d2,c,err
  character (len=200)             :: fname, outfname

! Read parameter file

  open(unit=1,file="dust-profile.par",status="old")
    ierr=0
    read(unit=1,fmt=*,iostat=ierr) fname
    read(unit=1,fmt=*,iostat=ierr) n
    read(unit=1,fmt=*,iostat=ierr) cenx
    read(unit=1,fmt=*,iostat=ierr) ceny
    read(unit=1,fmt=*,iostat=ierr) tmax
    read(unit=1,fmt=*,iostat=ierr) arcppix
    read(unit=1,fmt=*,iostat=ierr) outfname
  close(unit=1)

! allocate arrays

  allocate(x(n),y(n),t(n),d(n))
  x=0.0; y=0.0; t=0.0; d=0.0

! load data
 
  call load_file(fname,n,t,x,y)

! calculate D-values

  call dval(n,t,x,y,cenx,ceny,arcppix,tmax,d)

! count number of [t < tmax] data

  where (t < tmax)
    x=1
  elsewhere
    x=0
  endwhere
  nt=sum(x)
  print *,"-- Number of events in time interval:",nt

! allocate arrays for binned data

  nb=int(real(nt)/real(cpbin))
  allocate(d1(nb),d2(nb),c(nb),err(nb))
  d1=0.0; d2=0.0; c=0.0; err=0.0
  print *,"-- Number of bins:",nb

! Sort into accending order

  call sort2(nt,d(1:nt))

! bin up D-values to N>20 per bin

  call bind(nt,d(1:nt),nb,d1,d2,c,err,cpbin)

! save data

  call dump_data(outfname,d1,d2,c,err,nb)

END PROGRAM dustprofile

! --------------------------------------

SUBROUTINE load_file(fname,n,t,x,y)

  implicit none
  character (len=40)               :: fname
  integer                          :: n, i, ierr
  real, dimension(n)               :: t, x, y
  real                             :: d1, d2, d3

! Clear arrays

  t=0.0; x=0.0; y=0.0  

! Read in columns of numbers and put them in arrays 

  open(unit=1,file=fname,status="old")
  ierr=0; d1=0.0; d2=0.0
  do i=1,n
    if(ierr==0) then
      read(unit=1,fmt=*,iostat=ierr) d1, d2, d3
      t(i)=d1
      x(i)=d2
      y(i)=d3
    else
      exit
    endif
  enddo
  close(unit=1)

  print *,"-- Loaded",n,"data points"

END SUBROUTINE load_file

! -------------------------------------

SUBROUTINE dval(n,t,x,y,cenx,ceny,arcppix,tmax,d)

  implicit none
  integer                          :: n
  real, dimension(n)               :: t, x, y, d, theta2
  real                             :: cenx, ceny, arcppix, tmax

! calculate theta^2 = (X-cenX)^2 + (Y-cenY)^2 [in arcsec^2 units]

  x=x-cenx
  y=y-ceny
  theta2=(x**2+y**2)*(arcppix)**2

! calculate D-values = 827*(t-t0)/theta^2 parsec

  where (t < tmax)
!    d=827.0*t/theta2
     d=t/theta2
  elsewhere
    d=0.0
  endwhere

END SUBROUTINE dval

! --------------------------------------

SUBROUTINE dump_data(fname,d1,d2,d3,d4,n)

! Save data to an ASCII file

  implicit none
  integer                          :: n, a
  real, dimension(n)               :: d1, d2, d3, d4
  character (len=40)               :: fname

  open(unit=2,file=fname,status="new")
  do a=1,n
    write(unit=2,fmt="(2x,4(e15.6))") d1(a), d2(a), d3(a), d4(a)
  enddo
  close(unit=2)

END SUBROUTINE dump_data

! --------------------------------------

SUBROUTINE bind(n,d,nb,d1,d2,c,err,cmax)

  implicit none
  integer                :: n, nb, i, bin, cmax
  real, dimension(n)     :: d
  real, dimension(nb)    :: d1, d2, c, err

  d1=0.0; d2=0.0; c=0.0; err=0.0

  bin=1
  d1(bin)=d(1)
  do i=1,n
    c(bin)=c(bin)+1.0
    if (c(bin) >= real(cmax)) then
      d2(bin)=d(i)
      if (bin >= nb) exit
      bin=bin+1
      d1(bin)=d(i)
    endif
  enddo

  err=sqrt(c)

END SUBROUTINE bind

! --------------------------------------

SUBROUTINE sort2(n,arr)

! Sorts an array arr(1:n) into accending order using Quicksort, while
! making the corresponding rearrangements of the array brr(1:n)

  implicit none
  integer, parameter   :: M=7,NSTACK=50
  integer              :: n,i,ir,j,jstack,k,l
  integer, dimension(NSTACK) :: istack
  real, dimension(n)   :: arr,brr
  real                 :: a,b,temp

  brr=0.0

  jstack=0
  l=1
  ir=n
   
1 if ((ir-l) < M) then
out:  do j=l+1,ir
      a=arr(j)
      b=brr(j)
in:   do i=j-1,1,-1
        if (arr(i) < a) then
          goto 2
        endif
        arr(i+1)=arr(i)
        brr(i+1)=brr(i)
      enddo in
      i=0
2     arr(i+1)=a
      brr(i+1)=b
    enddo out
    if (jstack == 0) then
      return
    endif
    ir=istack(jstack)
    l=istack(jstack-1)
    jstack=jstack-2
  else
    k=(l+ir)/2
    temp=arr(k)
    arr(k)=arr(l+1)
    arr(l+1)=temp
    temp=brr(k)
    brr(k)=brr(l+1)
    brr(l+1)=temp
    if (arr(l+1) > arr(ir)) then
      temp=arr(l+1)
      arr(l+1)=arr(ir)
      arr(ir)=temp
      temp=brr(l+1)
      brr(l+1)=brr(ir)
      brr(ir)=temp
    endif
    if (arr(l) > arr(ir)) then
      temp=arr(l)
      arr(l)=arr(ir)
      arr(ir)=temp
      temp=brr(l)
      brr(l)=brr(ir)
      brr(ir)=temp
    endif
    if (arr(l+1) > arr(l)) then
      temp=arr(l+1)
      arr(l+1)=arr(l)
      arr(l)=temp
      temp=brr(l+1)
      brr(l+1)=brr(l)
      brr(l)=temp
    endif
    i=l+1
    j=ir
    a=arr(l)
    b=brr(l)
3   continue
     i=i+1
    if (arr(i) < a) then
      goto 3
    endif
4   continue
      j=j-1
    if(arr(j) > a) then
      goto 4
    endif
    if (j < i) then
      goto 5
    endif
    temp=arr(i)
    arr(i)=arr(j)
    arr(j)=temp
    temp=brr(i)
    brr(i)=brr(j)
    brr(j)=temp
    goto 3
5   arr(l)=arr(j)
    arr(j)=a
    brr(l)=brr(j)
    brr(j)=b
    jstack=jstack+2
! Push pointers to larger subarray on stack, process smaller subarray immediately    
    if (jstack > NSTACK) then
      print *,"NSTACK too small in sort2"
    endif
    if ((ir-i+1) > (j-l)) then
      istack(jstack)=ir
      istack(jstack-1)=i
      ir=j-1
    else
      istack(jstack)=j-1
      istack(jstack-1)=l
      l=i
    endif
  endif
  goto 1

END SUBROUTINE sort2

