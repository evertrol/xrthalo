#!/bin/csh
# -------------------------------------------------------
#
# dust-profile.csh by Simon Vaughan 2005
# C-shell script to extract dust n(D) profile from
# CCD (imaging mode) event list)
# uses: 
#   FtOOLS
#   /h/sav2/bin/dust-profile.f90

# Define what to do if interrupted

  onintr done
    echo -- line 14

# set script path

set scrdir = `pwd`
#/home/rlcs1/teaching/PA4970/dusthalos/050724tests
    echo -- line 19

# Initialise FTOOLS if not already done

  echo -- Initializing FTOOLS
#  lhea-setup-new

# define data parameters

  if ( $#argv < 5 ) then
    echo \*\* Need to specify :
    echo \*\* \(1\) FITS event file to use
    echo \*\* \(2\) x0 - detector x-position of GRB
    echo \*\* \(3\) y0 - detector y-position of GRB
    echo \*\* \(4\) t0 - burst time of GRB \(sec\)
    echo \*\* \(5\) tmax - max. time since GRB to use \(sec\)
    echo \*\* \(6\) arcsec per pixel \(e.g. 2.357 for Swift XRT\)
   goto done
  else
    set evtfile = $argv[1]
    set cenX = $argv[2]
    set cenY = $argv[3]
    set t0 = $argv[4]
    set tmax = $argv[5]
    set arcppix = $argv[6]
  endif

# subtract t0 from event times

  set oevtfile = $evtfile:r.t0
  fcalc infile=$evtfile+1 outfile=$oevtfile  clname="TIME" rowrange="-" inull=0 copycol=yes clobber=yes expr="TIME-$t0"

# convert FITS event file to ASCII table

  if ( -e $evtfile ) then
    echo -- Found file : $evtfile
    fdump infile=$oevtfile+1 outfile=temp.txt columns="TIME,X,Y" rows="-" prhead="no" showcol="no" showrow="no" showunit="no" clobber=yes
  else
    echo \*\* Cannot find file : $evtfile
    goto done
  endif

# remove leading two blank lines

  set outf = $evtfile:r.txt
  set nl = `cat temp.txt | wc -l`
  @ nl = $nl - 2
  tail -n $nl temp.txt >! $outf

# count number of events

  set ne = `cat $outf | wc -l`

# remove output file if exists

  set outp = $outf:r.dat
  if ( -e $outp ) then
    rm -f $outp
  endif

# write parameter file for dust-profile.f90

cat >! dust-profile.par <<EOF
${outf}
${nl}
${cenX}
${cenY}
${tmax}
${arcppix}
${outp}
EOF

# then run dust-profile.f90 to calculate n(D) distribution

  $scrdir/dust-profile

# convert output to XSPEC format

  set fpha = $outp:r.pha
  set frsp = $outp:r.rsp

  if ( -e $outp ) then
    echo -- Made output ASCII file : $outp
    flx2xsp infile=$outp phafil=$fpha rspfil=$frsp clobber=yes
  else
    echo \*\* No output ASCII file made by bin-profile.f90
    goto done
  endif
  
# load into XSPEC and plot

  cat >! $outp:r.xcm <<EOF
data ${fpha}
setplot energy
setplot command lw 3
setplot command font roman
setplot command la x Distance (pc)
setplot command la y Counts (pc \\\u-1\\\d)
setplot command line step
setplot command re x 
setplot command re y
plot ldata
setplot command hardcopy /ps
plot ldata
exit
EOF

    xspec - $outp:r.xcm 

# save output plot file

    if ( -e pgplot.ps ) then
      mv -f pgplot.ps $outp:r.ps
    endif
  else
    echo \*\* No PSD produced
    goto done
  endif

# all done

  done:
  echo -- All done
