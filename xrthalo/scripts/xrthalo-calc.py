#! /usr/bin/env python3.5

"""
TBD
"""


import os.path
import sys
import re
import argparse
import pydoc
import json
import logging
import glob
from collections import namedtuple
import requests
import numpy as np
from scipy.ndimage.filters import convolve
import matplotlib as mpl
from matplotlib import pyplot
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from astropy import units
from astropy.coordinates import SkyCoord
from astropy.time import Time
from astropy.io import fits as pyfits
import astropy.wcs
from astropy.utils.exceptions import AstropyWarning
from scipy.ndimage.measurements import center_of_mass
from scipy.ndimage.interpolation import zoom as ndimage_zoom
import aplpy
from xrthalo.data import datatype, bintype
from xrthalo.process import process_eventfile



# Ignore warnings from astropy, matplotlib and aplpy
# - aplpy warngs about missing WCS, which is ok for the dynamical image
# - matplotlib warngs about outdated (deprecated) usage, probably originated in aplpy
# - astropy warngs about non-standard header keywords in XRT data files
import warnings
warnings.simplefilter('ignore', category=mpl.cbook.MatplotlibDeprecationWarning)
logging.getLogger('aplpy').setLevel(logging.ERROR)
logging.getLogger('astropy').setLevel(logging.ERROR)
logging.getLogger('requests').setLevel(logging.WARNING)


class DataError(Exception):
    def __init__(self, *args, **kwargs):
        self.fail = kwargs.pop('fail', False)
        self.toofew = kwargs.pop('toofew', False)
        super().__init__(*args, **kwargs)


def calc_image_position(xpos, ypos, center, detx, dety, radius=30):
    """Create a 2D image from the event file and find the GRB position"""
    xpos, ypos = xpos.astype(np.int), ypos.astype(np.int)
    xcenter, ycenter = center
    shape = max(xpos)+1, max(ypos)+1
    image = np.zeros(shape, dtype=np.int)
    np.add.at(image, [xpos, ypos], 1)

    # Mask out events further away than `radius` pixels
    # xpos and ypos are np.int2, and run out of range when the square
    # becomes large (and xcenter or ycenter happen to be integer as
    # well)
    mask = ((xpos.astype(np.float) - xcenter)**2 +
            (ypos.astype(np.float) - ycenter)**2 <= radius*radius)
    image = np.zeros(shape, dtype=np.int)
    np.add.at(image, [xpos[mask], ypos[mask]], 1)
    xcenter, ycenter = center_of_mass(image)
    if np.isnan(xcenter) or np.isnan(ycenter):
        raise DataError("failed to determine source center", fail=True)

    return xcenter, ycenter


def calc_dust_profile(events, tmax, basename,
                      arcppix, dist0=827, cpbin=20, gtis=None):
    """Calculate and plot the dust profile

    Calculate the number of events as function of radius to the
    source. Any halo should show up as a bump on the (power-law)
    declining curve.

    """

    t = events['TIME']
    x = events['X']
    y = events['Y']
    theta2 = (x**2 + y**2) * arcppix**2
    #dist = np.zeros(x.shape, dtype=np.float)
    mask = t <= tmax
    mask &= events['PI'] >= 30
    dist = (dist0 * t / theta2)[mask]
    nevents = len(np.where(mask)[0])
    logging.info("Number of events within %.1f seconds: %d", tmax, nevents)

    nbins = int(nevents / cpbin)
    sdist = np.sort(dist)
    if nbins <= 1:
        raise DataError("too few bins for dust profile")

    low, high = sdist[::cpbin][:-1], sdist[::cpbin][1:]
    width = high - low
    binsize = cpbin * np.ones(low.shape)
    binsizeerror = np.sqrt(cpbin)
    binrate = binsize / width
    binrateerror = binsizeerror / width
    logcenter = np.exp((np.log(low) + np.log(high))/2)
    center = (low + high)/2

    figure = Figure((15, 10))
    canvas = FigureCanvas(figure)
    axes = figure.add_subplot(1, 1, 1)
    xsteps = np.append(low, [high[-1]])
    ysteps = np.append(binrate, [binrate[-1]])
    axes.errorbar(logcenter, binrate, binrateerror, fmt='.', color='#000000')
    axes.step(xsteps, ysteps, where='post', color='#666666')
    axes.grid()
    axes.set_xscale('log')
    axes.set_yscale('log')

    # splitext twice, to get rid of '.fits.gz'
    filename = basename + '-profile.png'
    canvas.print_figure(filename)
    pyplot.close(figure)

    # Save the ASCII data
    data = np.zeros(len(logcenter), dtype=bintype)
    filename = basename + '-profile.dat'
    with open(filename, 'w') as outfile:
        for x, y, error in zip(logcenter, binrate, binrateerror):
            data['x'] = x
            data['y'] = y
            data['e'] = error
            outfile.write("{:16g}  {:16g}  {:16g}\n".format(x, y, error))
    return data


def rebin(data, nbins):
    """Rebin with average, to a *maximum* of nbins

    Data are grouped by equal bins, up to a maximum of nbins, whatever
    fits closest.

    Any left over part after the last bin that does not fit in
    binsize, is thrown together with the last bin.

    """
    n = len(data)
    if n < nbins:
        return data
    binsize = int(np.ceil(n / nbins))
    nbins = n // binsize
    leftover = n % binsize
    lastbin = data[-leftover-binsize:].mean()
    data = data[:nbins*binsize].reshape(nbins, binsize).mean(axis=1)
    data[-1] = lastbin
    return data


def calc_dynamical_image(events, tmax, basename, reshape=(100, 200),
                         thetamax=10000, gtis=None, maxsize=4000000):
    """Calculate and create a dynamical image: time on the x-axis,
    distance to the source on the y-axis. Halo's should show up as
    diagonal lines.

    """

    # We need to convert to back from floating point: x and y event
    # data are 2-byte integers and will run out of range when squaring
    # (not automatic casting when using numpy arrays; cf using C). But
    # for indexing into the 2D image array, we need integers again.
    t = events['TIME']
    theta = events['X'].astype(np.float)**2 + events['Y'].astype(np.float)**2
    mask = (t < tmax) & (theta < thetamax)
    if not np.any(mask):
        raise DataError("no valid data for time & distance range")
    t = t[mask]
    theta = theta[mask]
    assert len(t) == len(theta)
    n = len(t)
    # Normalize to [0, n] and grid for image output
    t = n * t / np.max(t)
    t = t.astype(np.int64)
    stheta = n * theta / np.max(theta)
    stheta = stheta.astype(np.int64)
    shape = len(stheta)+1, len(t)+1
    image = np.zeros(shape, dtype=np.float)
    np.add.at(image, [stheta, t], 1)
    if shape[0] > reshape[0] and shape[1] > reshape[1]:
        nx, ny = image.shape[1] // reshape[0], image.shape[0] // reshape[1]
        newshape = ny*reshape[1], nx*reshape[0]
        zoomed_image = image[:newshape[0], :newshape[1]].reshape(
            reshape[1], ny, reshape[0], nx).mean(axis=3).mean(axis=1)
    else:
        zoomed_image = image
    #zoom = reshape[1] / image.shape[0], reshape[0] / image.shape[1]
    #zoomed_image = ndimage_zoom(image, zoom)
    filename = basename + '-dynimg.fits'
    pyfits.HDUList([pyfits.PrimaryHDU(zoomed_image)]).writeto(
        filename, clobber=True)
    if shape[0] * shape[1] > maxsize:
        print(shape, maxsize)
        logging.warning("Size of the dynamical image is too large "
                        "to create a PNG of it. Increase --maxsize if you want "
                        "to try anyway")
        return
    figure = Figure()
    canvas = FigureCanvas(figure)
    axes = figure.add_subplot(1, 1, 1)
    cmap = 'gray'
    axes.imshow(image**0.5, cmap='gray',
                extent=[0, np.max(t), 0, np.max(theta)],
                vmin=0, vmax=(image**0.5).max()/25., origin='lower',
                aspect='auto')
    axes.set_xlabel("Time (s)")
    axes.set_ylabel("Distance (pixel^2)")

    filename = basename + '-dynimg.png'
    canvas.print_figure(filename)
    return
    kernel = np.array([[0, 1, 0], [1, 3, 1], [0, 1, 0]])
    kernel = np.array([[1,4,7,4,1],[4,16,26,16,4],[7,26,41,26,7],
                       [4,16,26,16,4],[1,4,7,4,1]])
    zoomed_image = convolve(zoomed_image, kernel)
    figure = aplpy.FITSFigure(zoomed_image)
    figure.show_grayscale(vmin=0, vmax=zoomed_image.max()/25.,
                          stretch='power', exponent=0.5)
    figure.axis_labels.set_xtext("Time (s)")
    figure.axis_labels.set_ytext("Distance (pixel^2)")
    ticklabels = figure._ax1.get_xticklabels()
    nticks = 5
    step = 10*int(reshape[0]/10/nticks)
    ticks = np.arange(0, reshape[0]+1, step)
    labels = np.linspace(0, np.max(t), len(ticks))
    figure._ax1.set_xticks(ticks)
    figure._ax1.set_xticklabels(labels)
    figure._ax1.set_yticks(yticks)
    figure._ax1.set_yticklabels(ylabels)
    filename = basename + '-dynimg-smooth.png'
    figure.save(filename)
    figure.close()


def calc_lightcurve(events, tmax, basename, radius=20, cpbin=20, gtis=None):
    """Create the light curve with standard 20 counts/bin binning"""
    t = events['TIME']
    theta = events['X'].astype(np.float)**2 + events['Y'].astype(np.float)**2
    mask = (t < tmax) & (theta < radius*radius)
    t = t[mask]
    if len(t) <= cpbin:
        raise DataError("too few bins")
    if gtis is None:
        low, high = t[::cpbin][:-1], t[::cpbin][1:]
        width = high - low
        binsize = cpbin * np.ones(low.shape)
        binsizeerror = np.sqrt(cpbin)
        rate = binsize / width
        rateerror = binsizeerror / width
        center = (low + high)/2
    else:
        rate, rateerror, center = [], [], []
        start, stop = None, None
        for gti in gtis:
            if start is None:
                start = gti['START']
            if stop is None:
                stop = gti['STOP']
            mask = (t >= start) & (t <= stop)
            if len(np.flatnonzero(mask)) < cpbin:
                stop = None
                continue
            low, high = t[mask][::cpbin][:-1], t[mask][::cpbin][1:]
            width = high - low
            binsize = cpbin * np.ones(low.shape)
            binsizeerror = np.sqrt(cpbin)
            rate.extend(binsize / width)
            rateerror.extend(binsizeerror / width)
            center.extend((low + high)/2)
            start, stop = None, None
        rate = np.hstack(rate)
        rateerror = np.hstack(rateerror)
        center = np.hstack(center)

    figure = Figure((15, 10))
    canvas = FigureCanvas(figure)
    axes = figure.add_subplot(1, 1, 1)
    axes.errorbar(center, rate, rateerror, fmt='k-', marker='o',
                  color='#000000')
    axes.grid()
    axes.set_xscale('log')
    axes.set_yscale('log')

    filename = basename + '-lc.png'
    canvas.print_figure(filename)
    pyplot.close(figure)


def plot_image(events, tmax, center, wcs, basename, pixelsize,
               imgshape=None, radius=30):
    """Write the image to FITS and to PNG"""
    xpos, ypos = events['X']+center[0], events['Y']+center[1]
    xpos = xpos.round().astype(np.int)
    ypos = ypos.round().astype(np.int)
    if not imgshape:
        imgshape = max(ypos)+1, max(xpos)+1
    # Mask out positions that lie outside the image.
    mask = (xpos < imgshape[1]) & (ypos < imgshape[0])
    xpos = xpos[mask]
    ypos = ypos[mask]
    image = np.zeros(imgshape, dtype=np.int)
    np.add.at(image, [ypos, xpos], 1)

    img = pyfits.PrimaryHDU(image)
    header = wcs.to_header()
    table = dict(TCRPX2='CRPIX1', TCRVL2='CRVAL1',
                 TCDLT2='CDELT1', TCTYP2='CTYPE1',
                 TCRPX3='CRPIX2', TCRVL3='CRVAL2',
                 TCDLT3='CDELT2', TCTYP3='CTYPE2')
    for key, key2 in table.items():
        img.header[key2] = header[key]
    # +1: FITS images start at 1, numpy arrays at 0
    img.header['CRPIX1'] += 1
    img.header['CRPIX2'] += 1
    pyfits.HDUList([img]).writeto(basename + '-image.fits', clobber=True)

    figure = aplpy.FITSFigure(img)
    figure.show_grayscale(vmin=0, vmax=3, stretch='power', exponent=0.5)
    # APLpy likes it region input in celestial coordinates, not pixel
    ra, dec = figure.pixel2world(center[0], center[1])
    figure.recenter(ra, dec, width=0.3, height=0.3)
    figure.show_circles([ra], [dec], 2*radius/pixelsize/3600,
                        edgecolor='green', lw=2)
    filename = basename + '-image.png'
    figure.save(filename)
    figure.close()


##% def process_eventfile(grbdata, pixelsize, t_max, phamin=20, phamax=1000,
##%                       observations=None, maxsize=4000000):
##%     """Perform all calculations for a single event
##% 
##%     grbdata is a 'record' of type datatype, and corresponds to a
##%     single row in the info table.
##% 
##%     """
##% 
##%     logging.info("Processing %s", grbdata['grbname'])
##% 
##%     ra = grbdata['ra']
##%     dec = grbdata['dec']
##%     keys = ('TIME', 'X', 'Y', 'DETX', 'DETY', 'PI')
##%     events = dict([(key, []) for key in keys])
##%     maxshape = None
##%     gtis = []
##%     for i, evtfile in enumerate(grbdata['evtfiles'].split(';')):
##%         # splitext twice, to get rid of '.fits.gz'
##%         basename = os.path.splitext(os.path.splitext(evtfile)[0])[0]
##%         if observations is not None:
##%             observation = re.search(r'sw\d{8}(?P<obs>\d{3})', basename)
##%             if observation:
##%                 observation = int(observation.group('obs'))
##%                 if observation not in observations:
##%                     continue
##% 
##%         if not os.path.exists(evtfile):
##%             logging.warning("%s was not found", evtfile)
##%             continue
##%         logging.info("Processing file %s", evtfile)
##%         with pyfits.open(evtfile) as hdulist:
##%             hdu = hdulist[1]
##%             gtis.append(hdulist[2].data)
##%             # Get a rough estimate for the pixel centre from the
##%             # refined XRT position and the event-file WCS
##%             wcs = astropy.wcs.WCS(hdu.header, keysel=['pixel', 'pixel'],
##%                                   naxis=[1, 2])
##%             coords = wcs.wcs_world2pix([[ra, dec]], 1)
##%             xcenter, ycenter = coords[0]
##% 
##%             mask = (hdu.data['PI'] >= phamin) & (hdu.data['PI'] <= phamax)
##% 
##%             for key in keys:
##%                 events[key].append(hdu.data[key][mask].astype(np.float))
##% 
##%             try:
##%                 xcenter, ycenter = calc_image_position(
##%                     events['X'][-1], events['Y'][-1], coords[0],
##%                     events['DETX'][-1], events['DETY'][-1])
##%             except DataError as exc:
##%                 logging.warning(str(exc))
##%                 if exc.fail:
##%                     logging.error("Not processing file %s further", basename)
##%                     continue
##% 
##%             events['X'][-1] -= xcenter
##%             events['Y'][-1] -= ycenter
##%             imgshape = (hdu.header['TLMAX2'] - hdu.header['TLMIN2'] + 1,
##%                         hdu.header['TLMAX3'] - hdu.header['TLMIN3'] + 1)
##%             if maxshape:
##%                 if imgshape[0] > maxshape[0] and imgshape[1] > maxshape[1]:
##%                     maxshape = imgshape
##%             else:
##%                 maxshape = imgshape
##%             try:
##%                 plot_image({'X': events['X'][-1], 'Y': events['Y'][-1]},
##%                            t_max, (xcenter, ycenter), wcs, basename,
##%                            pixelsize, imgshape)
##%             except DataError as exc:
##%                 logging.warning(str(exc))
##% 
##%     if not len(events['TIME']):
##%         logging.info("No suitable data found. Ignoring this event.")
##%         return
##% 
##%     for key in keys:
##%         events[key] = np.hstack(events[key])
##%     logging.info("Combined number of events is %d", len(events['TIME']))
##%     gtis = np.hstack(gtis)
##%     basename = re.sub(r'(?P<pre>sw\d{8})(?P<repl>\d\d\d)', r'\g<pre>___',
##%                       basename)
##%     events['TIME'] -= grbdata['swiftseconds']
##%     gtis['START'] -= grbdata['swiftseconds']
##%     gtis['STOP'] -= grbdata['swiftseconds']
##% 
##% #    try:
##% #        dustprofile = calc_dust_profile(events, t_max, basename, pixelsize)
##% #    except DataError as exc:
##% #        logging.warning(str(exc))
##% #
##%     try:
##%         calc_dynamical_image(events, t_max, basename, maxsize=maxsize)
##%     except DataError as exc:
##%         logging.warning(str(exc))
##% #
##% #    try:
##% #        calc_lightcurve(events, t_max, basename, gtis=gtis)
##% #    except DataError as exc:
##% #        logging.warning(str(exc))
##% #
##% #    try:
##% #        plot_image(events, t_max, (xcenter, ycenter), wcs, basename,
##% #                   pixelsize, maxshape)
##% #    except DataError as exc:
##% #        logging.warning(str(exc))


class HelpAction(argparse.Action):
    """Help that pages the doc string when the long option is used
    (compare for example git)"""
    def __call__(self, parser, namespace, values, option_string=None):
        if option_string == '-h':
            parser.print_help()
            parser.exit()
        elif option_string == '--help':
            pydoc.pager(__doc__)
            parser.exit()


def parse_args():
    """Parse command line arguments"""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter, add_help=False)
    parser.add_argument('events', nargs='*', metavar='event',
                        help="event IDs or GRB names to use. "
                        "If not given, process all events.")
    parser.add_argument('--info-table',
                        default=os.path.abspath("xrt-info.csv"),
                        help="(CSV) file with table of all relevant data: "
                        "trigger id, grb name, trigger date, position, "
                        "event file name")
    parser.add_argument('--data-dir',
                        default=os.path.abspath("data"),
                        help="Data storage subdirectory")
    parser.add_argument('--observations', nargs='+', type=int,
                        help="Observation IDs to use")
    parser.add_argument('--tmin', default=0, type=float,
                        help="Minimum time since T0 for events to be used")
    parser.add_argument('--tmax', default=1e4, type=float,
                        help="Maximum time since T0 for events to be used")
    parser.add_argument('--phamin', default=20, type=int,
                        help="Lowest PHA channel to include")
    parser.add_argument('--phamax', default=1000, type=int,
                        help="Highest PHA channel to include")
    parser.add_argument('--swift-t0', default='2001-01-01T00:00:00',
                        help="Swift spacecraft zero time")
    parser.add_argument('--pixelsize', default=2.357, type=float,
                        help="Pixel size in arcseconds")
    parser.add_argument('--maxsize', default=4000000, type=int,
                        help="Maximum size (pixels, x*y) for the "
                        "dynamical image")
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help="Verbose level")
    parser.add_argument('-h', '--help', nargs=0, action=HelpAction,
                        help="Display this help and exit")

    args = parser.parse_args()
    grbs, events = [], []
    for event in args.events:
        if event.lower().startswith('grb'):
            grbs.append(event.upper())
        else:
            events.append(int(event))
    args.events = events
    args.grbs = grbs
    args.swift_t0 = Time(args.swift_t0)
    return args


def main():
    args = parse_args()
    loglevel = ['WARNING', 'INFO', 'DEBUG'][args.verbose]
    logging.basicConfig(level=loglevel,
                        format='%(asctime)s [%(levelname)s]: %(message)s',
                        datefmt='%Y-%m-%dT%H:%M:%S')

    alldata = np.genfromtxt(args.info_table, dtype=datatype, delimiter=',',)

    # process the event file, looking for a possible dust echo
    for i, grbdata in enumerate(alldata):
        if args.events:
            if args.grbs:
                if (grbdata['triggerid'] not in args.events and
                    grbdata['grbname'] not in args.grbs):
                    continue
            else:
                if grbdata['triggerid'] not in args.events:
                    continue
        if args.grbs:
            if grbdata['grbname'] not in args.grbs:
                continue

        process_eventfile(grbdata, args.pixelsize, args.tmax,
                          phamin=args.phamin, phamax=args.phamax,
                          observations=args.observations,
                          maxsize=args.maxsize)


if __name__ == '__main__':
    main()
