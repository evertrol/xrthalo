#! /usr/bin/env python3.5

"""Script to collect early XRT GRB data, to search for possible dust
haloes.

This script attempts to download all available XRT PC event files for
the first orbit (second or third orbit when there are no PC data in
the first orbit).




The detailed steps taken are as follows

- GRB XRT positions are found from
  http://www.swift.ac.uk/xrt_positions/?txt=1 Positions are converted
  to decimal degrees notation

- The GRB T0 data is obtained from
  http://gcn.gsfc.nasa.gov/swift_gnd_ana.html

  Note that there are more webpages linked at the end of the above
  webpage. Thus, there is no single look-up page, and in the future,
  more webpages may be added to the end of the file (and as a
  consequence, this script may need to be adjusted)..

  The linked webpages follow the convention
  http://gcn.gsfc.nasa.gov/swiftYYYY_gnd_ana.html

  Finally, the trigger ids and T0 data are obtained from the HTML of
  each page.

  The T0 data are also converted into Swift spacecraft seconds,
  starting at 0 seconds for 2001-01-01 midnight.

- The GRB names from the XRT position list are matched through
  http://www.swift.ac.uk/swift_portal/ . Note that sometimes the space
  between GRB and the number is necessary, at other times it's not, so
  both variations are tried.

  This result in a trigger id, which can be matched to the data with T0.

  The trigger id can also be used to determine the XRT event file, or
  if necessary, the complete XRT data set. The event file name is
  similar to

      sw00{TID}{ORB}xpc{WMODE}po_cl.evt

  where

    TID is the 6 digit number of the trigger id

    ORB is the 3 digit number for the orbit. Usually this is 000, but
    for a bright event, the first orbit may be windowed-timing mode
    only, and the file won't exist.

    WMODE is the standard PC mode, which is 'w3', but early in the
    mission was 'w4'.


- The script will loop through all XRT positions, matching them with
  T0. Any case where the position does not have a T0 (i.e., no
  matching trigger ID), or vice versa, are skipped over (with a notice
  issued).

  The final list of matches of writtin to a data file. This data can
  be re-used, avoiding unnecessary downloads of webpages in the
  future.

- The script will now loop through the list of matched records,
  attempting to download the XRT event file. Next orbits will be tried
  if the first orbit is not available, and other PC window modes are
  tried if the default is not available.

  The event file is saved into a working directory. If the file was
  already found there, no attempt at downloading the event file was
  taken, and the existing event file is used.

"""


import os.path
import re
import argparse
import pydoc
import json
import logging
import glob
from collections import namedtuple
import requests
import numpy as np
from astropy import units
from astropy.coordinates import SkyCoord
from astropy.time import Time
from astropy.io import fits
from xrthalo.data import datatype



# Define a local exception to easily catch problems
class DownloadError(Exception):
    pass


class DataError(Exception):
    def __init__(self, *args, **kwargs):
        self.fail = kwargs.pop('fail', False)
        self.toofew = kwargs.pop('toofew', False)
        super().__init__(*args, **kwargs)


def parse_xrt_positions(text):
    """Get the XRT positions from the downloaded webpage"""
    data = {}
    for line in text.split('\n'):
        fields = line.split('|')
        if len(fields) < 5:
            continue
        grbname = ''.join(fields[0].split())
        coord = SkyCoord(ra=fields[1], dec=fields[2],
                         unit=(units.hourangle, units.deg))
        data[grbname.upper()] = {'ra': coord.ra.value, 'dec': coord.dec.value}
    return data


def gather_xrtinfo(filename):
    """Get the XRT position information, from the web or an existing file"""
    if not os.path.exists(filename):
        logging.info("Finding XRT position info from "
                     "http://www.swift.ac.uk/xrt_positions/?txt=1")
        r = requests.get("http://www.swift.ac.uk/xrt_positions/?txt=1")
        xrtposdata = parse_xrt_positions(r.text)
        # Note: we lose some precision converting to JSON. That is
        # likely to be insignificant
        logging.info("Writing XRT position info to %s", filename)
        with open(filename, 'w') as outfile:
            json.dump(xrtposdata, outfile, indent=0)
    else:
        logging.info("Reading XRT position info from file %s",
                     filename)
        with open(filename) as infile:
            xrtposdata = json.load(infile)
    return xrtposdata


def parse_t0_info(text, swiftt0):
    parsing = False
    tid = 0
    date = ""
    time = ""
    data = {}
    for line in text.split('\n'):
        if not parsing:
            if '<table' in line.lower():
                parsing = True
            continue
        regex = re.search(r'(?P<tid>\d{6}).swift', line)
        if regex:
            tid = int(regex.group('tid'))
            continue
        regex = re.search(r'(?P<date>\d{2}\/\d{2}\/\d{2})', line)
        if regex:
            date = regex.group('date')
            continue
        regex = re.search(r'(?P<time>\d{2}:\d{2}:\d{2}\.\d{2})', line)
        if regex:
            time = regex.group('time')
            if tid and date and time:
                datetime = "20{}T{}".format(date.replace("/", "-"), time)
                t = Time(datetime, scale='utc')
                seconds = (t - swiftt0).sec   # Seconds spacecraft time
                data[tid] = {'datetime': datetime, 'swiftseconds': seconds}
            else:
                logging.warning("Missing info: %s, %s, %s", tid, date, time)
            tid = 0
            date = ""
            time = ""
    return data


def gather_t0info(filename, swiftt0):
    """Get the GRB t0 information, from the web or an existing file"""

    if not os.path.exists(filename):
        logging.info("Finding BAT T0 info from "
                     "http://gcn.gsfc.nasa.gov/swift_gnd_ana.html")
        r = requests.get("http://gcn.gsfc.nasa.gov/swift_gnd_ana.html")
        t0data = parse_t0_info(r.text, swiftt0)
        # This can get out-of-date easily; The status_code check is an
        # attempt to keep it up-to-date for the next few years.
        for year in range(2005, 2020):
            url = "http://gcn.gsfc.nasa.gov/swift{}_gnd_ana.html".format(year)
            r = requests.get(url)
            if r.status_code == 404:
                continue
            if r.status_code != 200:
                logging.warning("Unknown error when trying to retrieve %s",
                                url)
                continue
            t0data.update(parse_t0_info(r.text, swiftt0))
        logging.info("Writing BAT T0 data %s", filename)
        with open(filename, 'w') as outfile:
            json.dump(t0data, outfile, indent=0)
    else:
        logging.info("Reading BAT T0 data from file %s", filename)
        with open(filename) as infile:
            t0data = json.load(infile)
        # JSON (Javascript) can't use integers as a key; convert the
        # strings to ints after reading
        t0data = dict([(int(key), value) for key, value in t0data.items()])
    return t0data


def match_data(xrtposdata, t0data, swiftt0, events=None, grbs=None):
    """Match XRT positions with T0 times with GRB names

    Deduce possible event file names from the obtained matches

    """

    linkbase = "http://www.swift.ac.uk/swift_portal/searchname.php"
    alldata = np.empty(len(xrtposdata), dtype=datatype)
    i = 0
    for grbname, xrtpos in xrtposdata.items():
        if grbs and not events and grbname.upper() not in grbs:
            continue
        
        alldata[i]['grbname'] = grbname
        alldata[i]['ra'] = 0
        alldata[i]['dec'] = 0
        alldata[i]['T0'] = swiftt0
        alldata[i]['swiftseconds'] = 0
        alldata[i]['globfile'] = ""
        alldata[i]['evtfiles'] = ""
        
        name = grbname.lstrip('GRBgrb')
        datalink = "{}?name=GRB+{}&submit-Search_Names".format(linkbase, name)
        logging.info("Parsing %s", grbname)
        r = requests.get(datalink)
        if r.status_code == 404:  # Remove the space after 'GRB'
            datalink = "{}?name=GRB{}&submit-Search_Names".format(linkbase, name)
            r = requests.get(datalink)
            if r.status_code != 200:  # give up
                logging.warning("Can't look up GRB %s. " +
                                "Giving up and continuing with next", name)
                continue

        regex = re.search(r'archive/prepdata.php\?tid=(?P<tid>\d{6})', r.text)
        if not regex:
            logging.warning(
                "Can't find trigger id corresponding to GRB %s", name)
            continue
        tid = int(regex.group('tid'))

        # Match the found trigger id with the T0 data
        triggerids = []
        for triggerid, t0value in t0data.items():
            if events:
                if grbs:
                    if grbname not in grbs and triggerid not in events:
                        continue
                elif events not in events:
                    continue
            triggerids.append(triggerid)
            if triggerid == tid:
                alldata[i]['triggerid'] = triggerid
                alldata[i]['ra'] = xrtpos['ra']
                alldata[i]['dec'] = xrtpos['dec']
                alldata[i]['T0'] = t0value['datetime']
                alldata[i]['swiftseconds'] = t0value['swiftseconds']
                alldata[i]['globfile'] = "sw00{}{{}}xpc{{}}po_cl.evt.gz".format(
                    triggerid)
                alldata[i]['evtfiles'] = ""
                break
        else:
            if not events:
                logging.warning("Can't find T0 data for GRB %s", name)
        i += 1

    # Remove empty rows and save the data
    alldata = alldata[:i]

    return alldata


def gather_info(xrt_position_file, t0_file, swiftt0,
                events=None, grbs=None):
    """Gather the necessary information from the web

    - Grab the XRT position data
    - Grab the trigger time T0
    - Match the sets, and deduce event file names

    """
    logging.info("Gathering data from the web")

    xrtposdata = gather_xrtinfo(xrt_position_file)
    t0data = gather_t0info(t0_file, swiftt0)

    alldata = match_data(xrtposdata, t0data, swiftt0, events, grbs)

    return alldata


def download_xrt_data(tid, datadir, orbit):
    """Download XRT data for a given trigger ID tid and given orbit"""

    # Try and download an event file
    evtfiles = []
    for wmode in ('w3', 'w2', 'w4'):
        filename = "sw00{tid:d}{orbit:0>3d}xpc{wmode:s}po_cl.evt.gz".format(
            tid=tid, orbit=orbit, wmode=wmode)
        url = ("http://www.swift.ac.uk/archive/reproc/00{tid:d}{orbit:0>3d}/"
               "xrt/event/{filename:s}".format(
                   tid=tid, orbit=orbit, filename=filename))
        logging.debug("Trying %s", url)
        r = requests.get(url)
        if r.status_code == 404:
            continue  # try the next option
        evtfile = os.path.join(datadir, filename)
        with open(evtfile, 'wb') as outfile:
            outfile.write(r.content)
        evtfiles.append(evtfile)
    if not evtfiles:
        raise DownloadError("no XRT data found for {}, orbit {}".format(
            tid, orbit))
    return evtfiles


class HelpAction(argparse.Action):
    """Help that pages the doc string when the long option is used
    (compare for example git)"""
    def __call__(self, parser, namespace, values, option_string=None):
        if option_string == '-h':
            parser.print_help()
            parser.exit()
        elif option_string == '--help':
            pydoc.pager(__doc__)
            parser.exit()


def parse_args():
    """Parse command line arguments"""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter, add_help=False)
    parser.add_argument('events', nargs='*', metavar='event',
                        help="event IDs or GRB names to use. "
                        "If not given, process all events.")
    
#    parser.add_argument('-e', 'events', action='append', default=[],
#                        help="event ID (integer) or grb name to download. "
#                        "The option can be used multiple times. If the "
#                        "info-table does not exist, it is created for only "
#                        "these entries. If the info-table exists, only data "
#                        "files for these events are downloaded. If the event "
#                        "is selected by a trigger ID instead, all GRBs will "
#                        "have to be matched first (by download their info "
#                        "page, but *not* the actual data files) before the "
#                        "selection.")
    parser.add_argument('-o', '--orbit', nargs='+', default=[0,1,2], type=int,
                        help="Space separated list of orbits to download."
                        "Note: when using the short option, repeat the option "
                        "for each orbit")
    parser.add_argument('--info-table',
                        default=os.path.abspath("xrt-info.csv"),
                        help="File with table of all relevant data: "
                        "trigger id, grb name, trigger date, position, "
                        "event file name. Output file if not present.")
    parser.add_argument('--xrt-position-file',
                        default=os.path.abspath("xrt-positions.json"),
                        help="JSON file with the XRT positions, or "
                        "output file if not present.")
    parser.add_argument('--t0-data-file',
                        default=os.path.abspath("t0-data.json"),
                        help="JSON file with trigger T0 data, or"
                        "output file if not present.")
    parser.add_argument('--swift-t0', default='2001-01-01T00:00:00',
                        help="Swift spacecraft zero time.")
    parser.add_argument('--data-dir',
                        default=os.path.abspath("data"),
                        help="Data storage subdirectory")
    parser.add_argument('--no-download-data', action='store_true',
                        help="Do not attempt to download data")
    parser.add_argument('-v', '--verbose', action='count', default=0,
                        help="Verbose level")
    parser.add_argument('-h', '--help', nargs=0, action=HelpAction,
                        help="Display this help and exit")

    args = parser.parse_args()
    grbs, events = [], []
    for event in args.events:
        if event.lower().startswith('grb'):
            grbs.append(event.upper())
        else:
            events.append(int(event))
    args.events = events
    args.grbs = grbs
    args.swift_t0 = Time(args.swift_t0)
    return args


def main():
    args = parse_args()
    loglevel = ['WARNING', 'INFO', 'DEBUG'][args.verbose]
    logging.basicConfig(level=loglevel,
                        format='%(asctime)s [%(levelname)s]: %(message)s',
                        datefmt='%Y-%m-%dT%H:%M:%S')
    logging.getLogger('requests').setLevel(loglevel)

    filename = args.info_table
    if os.path.exists(filename):
        logging.info("Found existing info table: %s. "
                     "Using info from this file, instead of the web. ",
                     filename)
        alldata = np.genfromtxt(filename, dtype=datatype, delimiter=',')
    else:
        alldata = gather_info(args.xrt_position_file, args.t0_data_file,
                              args.swift_t0, args.events, args.grbs)
        logging.info("Saving gathered info to %s", filename)
        np.savetxt(filename, alldata,
                   fmt="%d,%s,%.2f,%s,%.12f,%.12f,%s,%s",
                   header="#evtid,grbname,t0seconds,t0UT,ra,dec,globpattern,"
                   "eventfiles")

    if args.no_download_data:
        return

    for grbdata in alldata:
        tid = grbdata['triggerid']
        grbname = grbdata['grbname'].upper()
        if args.events:
            if args.grbs:
                if grbname not in args.grbs and tid not in args.events:
                    continue
            elif tid not in args.events:
                continue
        elif args.grbs:
            if grbname not in args.grbs:
                continue

        logging.info("Parsing %s", grbdata['grbname'])

        datadir = os.path.join(args.data_dir, "{}".format(tid))
        os.makedirs(datadir, exist_ok=True)

        nevents = {}
        evtfiles = []
        for orbit in args.orbit:
            orbitglob = "{:?>3d}".format(orbit)
            globfile = grbdata['globfile'].format(orbitglob, '??')
            files = glob.glob(os.path.join(datadir, globfile))
            nevents[orbit] = 0
            if files:
                logging.info("Using pre-downloaded data for orbit %d", orbit)
                for evtfile in files:
                    nevents[orbit] += len(fits.open(evtfile)[1].data)
            else:
                logging.info("Attempting to download data for orbit %d", orbit)
                try:
                    files = download_xrt_data(tid, datadir, orbit)
                    for evtfile in files:
                        nevents[orbit] += len(fits.open(evtfile)[1].data)
                except DownloadError as exc:
                    logging.warning("%s", exc)
                    nevents[orbit] = -1
            evtfiles.extend(files)
        grbdata['evtfiles'] = ';'.join(evtfiles)

        # Always update the README
        with open(os.path.join(datadir, 'README'), 'w') as outfile:
            ra, dec = grbdata['ra'], grbdata['dec']
            coord = SkyCoord(ra, dec, unit=units.degree)
            outfile.write("""\
EventID = {}
Name = {}
T0 = {}
position = {}, {}
galactic = {}, {}
""".format(tid, grbname, grbdata['T0'], ra, dec, coord.galactic.l.value,
           coord.galactic.b.value, nevents))
            outfile.write("# of events:\n")
            for key in sorted(nevents.keys()):
                outfile.write("  orbit {:d}: {:d}\n".format(key, nevents[key]))

    # Save file with updated event files information
    np.savetxt(args.info_table, alldata,
               fmt="%d,%s,%.2f,%s,%.12f,%.12f,%s,%s",
               header="#evtid,grbname,t0seconds,t0UT,ra,dec,globpattern,"
               "eventfiles")


if __name__ == '__main__':
    main()
