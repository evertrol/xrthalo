from collections import namedtuple
import numpy as np


# Data types
#Tzerotype = namedtuple('Tzero', ['triggerid', 'datetime', 'swiftseconds'])
#XRTpostype = namedtuple('XRTpos', ['grbname', 'ra', 'dec'])
datatype = np.dtype([
    ('triggerid', np.int64),
    ('grbname', (np.str, 12)),
    ('swiftseconds', np.float64),
    ('T0', (np.str, 25)),
    ('ra', np.float64),
    ('dec', np.float64),
    ('globfile', (np.str, 255)),
    ('evtfiles', (np.str, 2048)),
    ])
#evttype = np.dtype([
#    ('reltime', np.float64),
#    ('xpixel', np.int32),
#    ('ypixel', np.int32),
#    ])
bintype = np.dtype([
    ('x', np.float64),
    ('y', np.float64),
    ('e', np.float64),
    ])

